#include <gtk/gtk.h>
#include <linux/input-event-codes.h>

static const gchar *
get_key_name (guint keycode)
{
  switch (keycode - 8) {
  case KEY_LEFTALT:
    return "alt";

  case KEY_UP:
    return "↑";

  case KEY_LEFT:
    return "←";

  case KEY_RIGHT:
    return "→";

  case KEY_DOWN:
    return "↓";
  }

  return "unknown";
}

static void
key_pressed_cb (GtkEventControllerKey *controller,
               guint                  keyval,
               guint                  keycode,
               GdkModifierType        state,
               gpointer               user_data)
{
  g_print ("pressed %s\n", get_key_name (keycode));
}

static void
key_released_cb (GtkEventControllerKey *controller,
                 guint                  keyval,
                 guint                  keycode,
                 GdkModifierType        state,
                 gpointer               user_data)
{
  g_print ("released %s\n", get_key_name (keycode));
}

static void
activate_cb (GApplication *app)
{
  GtkWidget *window;
  GtkEventController *controller;

  window = gtk_application_window_new (GTK_APPLICATION (app));
  controller = gtk_event_controller_key_new ();

  gtk_widget_add_controller (window, controller);

  g_signal_connect_object (controller, "key-pressed", G_CALLBACK (key_pressed_cb), NULL, 0);
  g_signal_connect_object (controller, "key-released", G_CALLBACK (key_released_cb), NULL, 0);

  gtk_widget_show (window);
}

gint
main (gint    argc,
      gchar **argv)
{
  GApplication *app;

  app = G_APPLICATION (gtk_application_new ("org.example.Keys",
                                            G_APPLICATION_FLAGS_NONE));

  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);

  return g_application_run (app, argc, argv);
}
